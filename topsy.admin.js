// From topsy Widget 1.2.7
function topsyTrackbackSettingsDisplayer() {
    if ($("#topsy_trackbacks_enabled").is(':checked')) {
        $("#topsy_trackback_comments_config").slideDown(850);
    } else {
        $("#topsy_trackback_comments_config").slideUp(850);
    }
}
function topsyMockupRebuilder(n) {
    if (n == null) n = 0;
    if (n > 10) return;
    if (! $('#topsy_example_with_badge .topsy-sm')) {
        setTimeout(function(){topsyMockupRebuilder(n + 1);}, 1000);
        return;
    }
    var order = $('#topsy_smallbutton_order option:selected').val();
    topsyMockupMakeOrderBig(order, $('#topsy_theme_sample_big .topsy-big'));
    topsyMockupMakeOrderSmall(order, $('#topsy_theme_sample_small .topsy-sm'));
    topsyMockupMakeOrderBigBadge(order, $('#topsy_theme_sample_big_badge .topsy-big'));
    topsyMockupMakeOrderSmall(order, $('#topsy_theme_sample_small_badge .topsy-sm'));
}
function topsyMockupMakeOrderBig(order, branch) {
    branch = branch.get(0);
    if (! branch) return;
    var count = $(branch).find('.topsy-big-total').get(0);
    var retweet = $(branch).find('.topsy-big-retweet').get(0);
    var items = order.split(',');
    
    if (jQuery.inArray('count', items) == -1) { $(count).hide(); } else { $(count).show(); }
    if (jQuery.inArray('retweet', items) == -1) { $(retweet).hide(); } else { $(retweet).show(); }
}
function topsyMockupMakeOrderBigBadge(order, branch) {
    branch = branch.get(0);
    if (! branch) return;
    var count = $(branch).find('.topsy-big-total-badge').get(0);
    var retweet = $(branch).find('.topsy-big-retweet').get(0);
    var badge = $(branch).find('.topsy-big-badge').get(0);
    var items = order.split(',');
    
    if (jQuery.inArray('count', items) == -1) { $(count).hide(); } else { $(count).show(); }
    if (jQuery.inArray('retweet', items) == -1) { $(retweet).hide(); } else { $(retweet).show(); }
    if (jQuery.inArray('badge', items) == -1) { $(badge).hide(); } else { $(badge).show(); }
}
function topsyMockupMakeOrderSmall(order, branch) {
    branch = branch.get(0);
    if (! branch) return;
    
    var count = $(branch).find('.topsy-sm-total').get(0);
    var retweet = $(branch).find('.topsy-sm-retweet').get(0);
    var badge = $(branch).find('.topsy-sm-badge').get(0);

    $([count, retweet, badge]).each(function(n, item) {
        if (item) $(item).hide();
    });
    $(order.split(',')).each(function(n, item) {
        switch (item) {
            case 'count':
                branch.appendChild(count);
                $(count).show();
                break;
            case 'retweet':
                branch.appendChild(retweet);
                $(retweet).show();
                break;
            case 'badge':
                if (badge !== undefined) {
                    branch.appendChild(badge);
                    $(badge).show();
                }
                break;
        }
    });
}
function topsyNewCss(code) {
    if (document.getElementById(code) == null) {
        var head    = document.getElementsByTagName('head')[0];
        var link    = document.createElement('link');
        link.rel    = 'stylesheet';
        link.type   = 'text/css';
        link.id     = 'topsy-widget-css-' + code;
        link.href   = 'http://cdn.topsy.com/css/widget.' + code + '.css';
        head.appendChild(link);
    }
}
function topsySetButtonTheme() {
    var theme = $('#topsy_button_theme option:selected').val();
    $('#topsy_theme_sample_small').attr("className", function() { return this.className.replace(/topsy_theme_[a-z0-9-]+/, 'topsy_theme_' + theme); });
    $('#topsy_theme_sample_big').attr("className", function() { return this.className.replace(/topsy_theme_[a-z0-9-]+/, 'topsy_theme_' + theme); });
    $('#topsy_theme_sample_small_badge').attr("className", function() { return this.className.replace(/topsy_theme_[a-z0-9-]+/, 'topsy_theme_' + theme); });
    $('#topsy_theme_sample_big_badge').attr("className", function() { return this.className.replace(/topsy_theme_[a-z0-9-]+/, 'topsy_theme_' + theme); });
}
function topsyUpdateButtonText() {
    $('#topsy_theme_sample_small a.topsy-sm-retweet').text($('#topsy_button_text_retweet').val());
    $('#topsy_theme_sample_big a.topsy-big-retweet').text($('#topsy_button_text_retweet').val());
    $('#topsy_theme_sample_small_badge a.topsy-sm-retweet').text($('#topsy_button_text_retweet').val());
    $('#topsy_theme_sample_big_badge a.topsy-big-retweet').text($('#topsy_button_text_retweet').val());
}
function topsyCheckHashtags(page_init) {
    if ($("#topsy_auto_hashtags_custom").is(':checked')) {
        $("#topsy_auto_hashtags_text").attr('disabled', false);
        $("#topsy_auto_hashtags_text").removeClass('topsy_disabled');
        if (page_init !== true) $("#topsy_auto_hashtags_text").focus();
    } else {
        $("#topsy_auto_hashtags_text").attr('disabled', 'disabled');
        $("#topsy_auto_hashtags_text").addClass('topsy_disabled');
    }
}
$(document).ready(function() {
    $('.topsy_trackbacks_trigger').click(topsyTrackbackSettingsDisplayer);
    $('#topsy_smallbutton_order').mouseup(topsyMockupRebuilder);
    $('#topsy_smallbutton_order').keyup(topsyMockupRebuilder);
    $('#topsy_smallbutton_order').change(topsyMockupRebuilder);
    $('#topsy_button_theme').mouseup(topsySetButtonTheme);
    $('#topsy_button_theme').keyup(topsySetButtonTheme);
    $('#topsy_button_theme').change(topsySetButtonTheme);
    $('#topsy_auto_hashtags_none').click(topsyCheckHashtags);
    $('#topsy_auto_hashtags_post').click(topsyCheckHashtags);
    $('#topsy_auto_hashtags_custom').click(topsyCheckHashtags);
    $('#topsy_button_text_retweet').keyup(topsyUpdateButtonText);
    setTimeout(function() {
        topsyMockupRebuilder();
        topsyTrackbackSettingsDisplayer();
        topsySetButtonTheme();
        topsyCheckHashtags(true);
    }, 1500);
});    