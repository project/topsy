<?php

/**
 * @file
 * Administrative page callbacks for the Topsy module.
 */

/**
 * Form builder. Builds and returns the Administration settings form.
 *
 * @ingroup forms
 * @see system_settings_form()
 *
 */
function topsy_admin_settings() {
  topsy_update_theme_list();
  topsy_add_settings_js();

  /*
   * Organized in 3 groups of settings.
   * Each one has its own fieldset and is saved as an array
   * instead of spare variables thanks to #tree property.
   *
   * The default values are set in topsy_get_settings() (file topsy.module)
   *
   */
  $topsy_node_type_settings = topsy_get_settings('node_type');
  $topsy_retweet_settings = topsy_get_settings('retweet');
  $topsy_button_settings = topsy_get_settings('button');

  // This is the only option not in an array to speed up the decision
  // of displaying the button in hook_nodeapi and others.
  $form['topsy_display_option'] = array(
    '#type'  => 'select',
    '#title' => t('Method to display the button'),
    '#options' =>  array(
      'content' => t('Content'),
      'link' => t('Links area'),
      'other' => t('Other'),
    ),
    '#default_value' => variable_get('topsy_display_option', 'content'),
    '#description' => t('Default value: Content. If you are using other methods like displaying in a block, as a CCK field / View or direct PHP calls, choose "Other".'),
  );

  //********************  Topsy Node Type Settings
  $node_types = node_get_types('names');

  $form['topsy_node_type_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Node type settings'),
    '#tree' => TRUE,
    '#description' => t('This group "Node type settings" only takes effect if you have chosen to display the button in "Content" or "Links area."'),
  );
  $form['topsy_node_type_settings']['enabled'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display button on'),
    '#options' => $node_types,
    '#default_value' => $topsy_node_type_settings['enabled'],
    '#description' => t('In what content types you want to show the retweet button.'),
  );
  $form['topsy_node_type_settings']['teaser'] = array(
    '#type' => 'select',
    '#title' => t('Teaser view'),
    '#options' => array('big' => t('Big'), 'small' => t('Small'), 'hidden' => t('Hidden')),
    '#default_value' => $topsy_node_type_settings['teaser'],
    '#description' => t('Size of the button in teaser mode.'),
  );
  $form['topsy_node_type_settings']['full'] = array(
    '#type' => 'select',
    '#title' => t('Full node view'),
    '#options' => array('big' => t('Big'), 'small' => t('Small'), 'hidden' => t('Hidden')),
    '#default_value' => $topsy_node_type_settings['full'],
    '#description' => t('Size of the button in full node mode.'),
  );
  $form['topsy_node_type_settings']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => $topsy_node_type_settings['weight'],
    '#description' => t('Heavier items will sink. Default value -20 will show the button at the top of the node content.(If you have choosen display in "Links Area", the weight does not affect to the position)'),
  );


  //********************  Topsy Button Settings
  $all_smallbutton_orders = array(
        'count',
        'retweet',
        'count,retweet',
        'retweet,count',
        'count,badge',
        'badge,count',
        'retweet,badge',
        'badge,retweet',
        'count,retweet,badge',
        'count,badge,retweet',
        'retweet,count,badge',
        'retweet,badge,count',
        'badge,count,retweet',
        'badge,retweet,count',
  );

  $form['topsy_button_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Button settings'),
    // Change some CSS attributes in the form
    '#after_build' => array('topsy_change_form_attributes'),
    '#tree' => TRUE,
  );
  $form['topsy_button_settings']['theme'] = array(
    '#type' => 'select',
    '#title' => t('Button theme'),
    '#default_value' => $topsy_button_settings['theme'],
    '#options' => variable_get('topsy_button_theme_list', ''),
    '#description' => theme_topsy_button_preview($topsy_button_settings['theme'], $topsy_button_settings['text_retweet']),
  );
  $form['topsy_button_settings']['order'] = array(
    '#type' => 'select',
    '#title' => t('Element Selection and Order'),
    '#default_value' => $topsy_button_settings['order'],
    '#options' => drupal_map_assoc($all_smallbutton_orders),
    '#description' => t('Select which elements you want in your button (tweet count, retweet button, and Toplinks badge).'),
  );
 /* // Not working. I don't know if it's a bug or will be deprecated
  * // this option. Always is used text_retweet (the next form item)
  * $form['topsy_button_settings']['text_tweet'] = array(
    '#type' => 'textfield',
    '#title' => t('Text for "Tweet" button'),
    '#default_value' => $topsy_button_settings['text_tweet'],
    '#description' => t('Button text when node has not been still tweeted'),
  );
  */
  $form['topsy_button_settings']['text_retweet'] = array(
    '#type' => 'textfield',
    '#title' => t('Text for "Retweet" button'),
    '#default_value' => $topsy_button_settings['text_retweet'],
    '#description' => t('Button text. By default "retweet".'),
  );
  $form['topsy_button_settings']['css'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional CSS'),
    '#default_value' => $topsy_button_settings['css'],
    '#description' => t('Use semicolons ; to separate the CSS rules. By default will be shown floating in the right: <em>margin: 0 1em 1em 1em;float:right;</em>. '),
  );
  $form['topsy_button_settings']['streaming'] = array(
    '#type' => 'checkbox',
    '#title' => t('Streaming'),
    '#default_value' => $topsy_button_settings['streaming'],
    '#description' => t('Automatically update retweet counts in real time.'),
  );
  $form['topsy_button_settings']['preloading'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preload Retweet Button'),
    '#default_value' => $topsy_button_settings['preloading'],
    '#description' => t('Preload static retweet button before fetching count/badge data.  It can make  rendering HTML page slower depending of the browser.'),
  );


   //********************  Topsy Retweet Settings

  $services['topsy'] = t('Topsy - bit.ly');
  $services['node'] = t('Own site: /node/xyz');
  if (module_exists('shorten')) {
    $services['shorten'] = t('Use Shorten Module. Aliased');
  }
  $services['none'] = t('None');

  $form['topsy_retweet_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Retweet Settings'),
    '#tree' => TRUE,
  );
  $form['topsy_retweet_settings']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Retweet Username'),
    '#default_value' => $topsy_retweet_settings['username'],
    '#description' => t('Clicking on the Retweet button, the text "RT @username" will be placed at the beginning of the tweet.'),
  );
  $form['topsy_retweet_settings']['alias'] = array(
    '#type' => 'checkbox',
    '#title' => t('Aliased URLs'),
    '#default_value' => $topsy_retweet_settings['alias'],
    '#description' => t('Use Aliased URLs for shortening. By default enabled. Good for SEO, but be careful if you change the path alias of the node: counting will start from 0 and previous generated short links could stop working.'),
  );
  $form['topsy_retweet_settings']['shortener'] = array(
    '#type' => 'select',
    '#title' => t('Service'),
    '#description' => t('The default service to use to create the abbreviated URL.'),
    '#default_value' => $topsy_retweet_settings['shortener'],
    '#options' => $services,
    '#description' => t('"Topsy - bit.ly" will make URL shortened by Topsy using bit.ly on the fly when clicking "retweet". "Own site" will make that your shorts URLs like /node/34. And also you can use <a href="@status">Shorten module</a> to specify other Shortener services. More info in README.txt.', array('@status' => url(' http://drupal.org/project/shorten'))),
  );
  $form['topsy_retweet_settings']['hashtags'] = array(
    '#type' => 'textfield',
    '#title' => t('Automatic hashtags'),
    '#default_value' => $topsy_retweet_settings['hashtags'],
    '#description' => t('Will be appended to the end of the tweet automatically. Separate them by comma and do not write the #.'),
  );
  $form['topsy_retweet_settings']['baseurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Base url'),
    '#default_value' => $topsy_retweet_settings['baseurl'],
    '#description' => t('<strong>Leave empty</strong> unless you have any problem. Can be useful for developers or when you need a common base url across domains or subdomains. Example: http://mydomain.com (without last slash).'),
  );

  return system_settings_form($form);
}

/*
 * Validate input
 */
function topsy_admin_settings_validate($form, &$form_state) {
  // Helper function defined in topsy.module
  topsy_trim_and_polish($form_state['values']['topsy_retweet_settings']['hashtags'], ',');
  topsy_trim_and_polish($form_state['values']['topsy_button_settings']['css'], ';');
}

/**
 * Change the attributes of some form elements
 */
function topsy_change_form_attributes($form_element, &$form_state) {
  $form_element['text_tweet']['#id'] = 'topsy_button_text_tweet';
  $form_element['text_retweet']['#id'] = 'topsy_button_text_retweet';
  $form_element['theme']['#id'] = 'topsy_button_theme';
  $form_element['order']['#id'] = 'topsy_smallbutton_order';
  return $form_element;
}

/**
 * Theme the HTML of the example button in the admin settings page.
 */
function theme_topsy_button_preview($theme = 'blue', $text = 'retweet') {
  $output = '';
  $output .= '<div style="margin-top: 2em;">';
  $output .= '<div style="float: right;padding:1em;margin-left: 3em;font-size:1.2em;border: thin groove #999;">Your Topsy buttons will look like this:<br /><br />';
  $output .=   '<div id="topsy_theme_sample_big" class="topsy_widget_data topsy_theme_' . $theme . '" style="float: left; margin-right: 3em;"><!-- { "url": "http://topsy.com", "retweet_text": "' . $text . '", "style": "big" } --></div>';
  $output .=   '<div id="topsy_theme_sample_small" class="topsy_widget_data topsy_theme_' . $theme . '" style="float: left; margin-right: 3em;"><!-- { "url": "http://topsy.com", "retweet_text": "' . $text . '", "style": "small" } --></div>';
  $output .=   '<br clear="all" /><br />Versions with Toplinks badges:<br /><br />';
  $output .=   '<div id="topsy_theme_sample_big_badge" class="topsy_widget_data topsy_theme_' . $theme . '" style="float: left; margin-right: 3em;"><!-- { "url": "http://lifehacker.com/5401954/programmer-101-teach-yourself-how-to-code", "retweet_text": "' . $text . '", "style": "big" } --></div>';
  $output .=   '<div id="topsy_theme_sample_small_badge" class="topsy_widget_data topsy_theme_' . $theme . '" style="float: left; margin-right: 3em;"><!-- { "url": "http://lifehacker.com/5401954/programmer-101-teach-yourself-how-to-code", "retweet_text": "' . $text . '", "style": "small" } --></div>';
  $output .= '</div></div>';
  return $output;
}

/**
 * Add the javascript needed for change in real time the theme of the
 * preview button.
 * Part of the javascript code is dinamically generated.
 */
function topsy_add_settings_js() {
  // This javascript file needs to be added with drupal_set_html_head()
  // because is a external javascript file.
  // Also with drupal_add_js is not possible to specify the id attribute
  drupal_set_html_head("<script type=\"text/javascript\" id=\"topsy-js-elem\" src=\"http://cdn.topsy.com/topsy.js?init=topsyWidgetCreator\"></script>\n");

  $path = drupal_get_path('module', 'topsy');
  drupal_add_js($path . '/topsy.admin.js');

  // Prepare the javascript to add.
  $themes = variable_get('topsy_button_theme_list', '');
  if (is_array($themes)) {
    $temp_js = "";
    foreach ($themes as $theme_name) {
      $temp_js .= " topsyNewCss('" . topsy_simplify($theme_name) . "');\n";
    }
    $js_inline = "$(document).ready(function() {\n $temp_js });";
    drupal_add_js($js_inline, 'inline');
  }
}

/**
 * Return a machine-name from a string.
 */
function topsy_simplify($str) {
    $str = check_plain(trim(drupal_strtolower($str)));
    $str = preg_replace("/[^ \w-]/", '', $str);
    $str = preg_replace("/[^a-z0-9]+/", '-', $str);
    return $str;
}

/**
 * Creates an associative array from an indexed one.
 * Different way than drupal_map_assoc function.
 */
function topsy_map_assoc($array) {
  $result = array();
  foreach ($array as $value) {
    $result[topsy_simplify($value)] = $value;
  }
  return $result;
}

/**
 * Update and cache a list of possible themes for the button.
 * It's done parsing a external json resource from topsy host to an array.
 * The constants used are defined at the beginning of topsy.module file
 */
function topsy_update_theme_list() {
  $topsy_button_theme_list_cachetime = variable_get('topsy_button_theme_list_cachetime', 0);

  // Abort if has been updated recently. Default 3600 secs
  if (time() - $topsy_button_theme_list_cachetime < TOPSY_CACHE_TIME_UDATA) return;
  $http_data = topsy_http_get(TOPSY_THEME_LIST);
  if ($http_data->code < 200 || $http_data->code > 299) {
    watchdog('Topsy Retweet', "Couldn't update URL shortener data; queried %data_resource_url  data_resource_url and got back a %code in response." , array('%data_resource_url' => $data_resource_url, '%code' => $http_data->code) , WATCHDOG_ERROR);
    return;
  }
  if (isset($http_data->error)) return;

  // Process the received list of themes, sanatize the values and save them.
  $new_theme_list = @json_decode($http_data->data);
  $sett = topsy_get_settings('button');
  $cur_theme = $sett['theme'];
  $cur_theme_included = FALSE;

  if (is_array($new_theme_list)) {
    $sanatized = array();
    foreach ($new_theme_list as $theme) {
      $sanatized[] = check_plain($theme);
      if (topsy_simplify($theme) == $cur_theme) {
        $cur_theme_included = TRUE;
      }
    }
  }

  if (!$cur_theme_included) {
    array_push($sanatized, topsy_simplify($cur_theme));
  }
  variable_set('topsy_button_theme_list', topsy_map_assoc($sanatized));
  variable_set('topsy_button_theme_list_cachetime', time());
}
