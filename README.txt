
-- SUMMARY --

  * The Topsy Retweet Button module provides a retweet button that shows tweets
    count and allows retweeting.

-- REQUIREMENTS --

  * php 5.2
    
    
-- FEATURES --
    
  * Highly customizable buttons can be displayed in various colors 
    and two sizes.      
  * Can be displayed:
    - In the node content
    - In the links area
    - In a block
    - As a CCK field (with Views Integration)  
  * CCK field implemented as a separate module lets user decide
    displaying the Topsy Retweet button in each node.
  * Automatic URL shortening using bit.ly    
  * Other URL shortening services can be used if you install Shorten module.
    http://drupal.org/project/shorten      
  * Displays Topsy TopLinks badges for posts that are in the
    Topsy Top5k or above - http://labs.topsy.com/toplinks      
  * Topsy doesn't delete old tweets, so the plugin will work on all
    of your historical posts.          
  * Allows you to choose the username used as the source for retweets
    from your blog - e.g. "RT @<your Twitter account> ..."     
  * No module dependencies.    
  * Automatic #hashtags, option for streaming, preloading, and more.        


-- INSTALLATION (EASY WAY) AND BASIC USAGE --

  * Place the "topsy" directory into a modules directory of your choice,
    in most cases: sites/all/modules.
    
  * Enable "Topsy Retweet Button" module by navigating to:  
    Administer > Site building > Modules    
    Note. For this easy way, it's not needed the "CCK Topsy Retweet Button".
    
  * Enable and configure where to show the button by visiting:
    Administer > Site configuration > Topsy Retweet Button

  * Choose what roles have the permission to "view the button"
    Administer > User Management -> Permissions

  * Done! You will be able to see the button now.    
    
  * It will display the counting of the totals tweets associated to that node,
    including older tweets that were not necessarily generated with Topsy 
    Retweet Button.    
    
  * Clicking on the count number will take you to http://topsy.com where you 
    can see all the tweets associated to that URL. (ALL of them, Topsy don't
    delete old tweets).
    
  * Also clicking "retweet" in the button, should take you to 
    http://twitter.com and fill the message box with a text like this:
    "RT @TopsyRT: Sed Wisi Hos http://bit.ly/7al2Qj"
    (still you have the chance to modify it before "Tweet" it)
    

--  ALTERNATIVE WAYS 
  
  NOTE. If you are not using the "standard" way, it's recommended to 
  choose in the general settings "Method to display the button": Other
  You will obtain a minor performance gain.    
    
  -- From a block
    * Enable the block "Topsy Retweet Button". 
    * Clicking "Configure" you will be able to set the size of the button.
    * Note that *only* will appear when you are in a node page, and the
      button will reference the node that is being displayed in that moment.
         
  -- As a CCK field / Views
    * Enable the module  "CCK Topsy Retweet Button", that provides an
      CCK field. It depends on the "Topsy Retweet Button" module.
    * Biggest difference is that you have more granulated control when 
      to show the button by marking a checkbox when creating or editing
      a node. The default value is "show".
    * Also it will provide automatically a field (and filter) for Views, 
      called "Content: field_(name that you give)", 
  
  -- For themers:     
     If topsy was enabled the "easy way", then in node.tpl.php you can use
     this to print the button:
         <?php print $node->content['topsy_button']['#value']; ?>     
         
  -- For developers: 
    * Use the function theme_topsy_button($settings); 
    * Have a look directly to topsy.module and see the function documentation.
           
           
           
-- ABOUT SHORTENING OPTIONS

  * "Own site" option.
    If you want the non-aliased node paths as a short URL.
    http://yourdomain.com/node/32. It will create that path even if you
    have aliases for the node. That's why is recommended to use along with
    "Global Redirect" module, so it redirects to the aliased path.

  * "Using Shorten Module" option.
    Shorten Module provides you a lot of more Shorten services and the
    possibility to set up your own account of bit.ly.
    
    NOTE:  When Shorten Module is selected,  short URLs are generated 
    (and cached!) the first time the Retweet Button is loaded for a given
    node. And Shorten Module doesn't provide a UI to empty the cache. 
    
    ****** So if you are not used to manipulate database tables manually ***
    choose first *carefully* the administration settings in Shorten Module
    and THEN, select "Shorten module" as a method for URL shortening in
    Topsy Retweet Settings.


-- FINAL NOTE  / TROUBLESHOOTING

  * If at any moment you change the URL of the node (maybe your domain, 
    sub domain, clean URL, path alias, etc.. ) the counting might be affected too, 
    and it will star from 0 again, because it is always associated to the 
    current node page URL.
    
  * Button will be not displayed if your browser has JavaScript disabled.
    
  * If you can't see the button or the behavior of clicking "retweet" is not 
    as described, fill an issue with your problem and even better,
    a link to your site.    
    http://drupal.org/project/issues/topsy?status=All&categories=All
        
   
-- CONTACT --

  * David Corbacho 'corbacho' - http://drupal.org/user/420096

  This project has been sponsored by:

  * Drupro   
    Rapid Drupal Development.
    http://www.drupro.com
    
    